# BASH Prompt
export PS1="\[\e[0;36m\]\w: \[\e[0m\]"

# Aliases
alias aptup='sudo apt-get update && sudo apt-get upgrade'
alias ls='ls --color=auto'
